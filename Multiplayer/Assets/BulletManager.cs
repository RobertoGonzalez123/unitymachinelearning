using System.Collections;
using System.Collections.Generic;
using Unity.Netcode;
using UnityEngine;

public class BulletManager : MonoBehaviour
{
    Vector3 moveDir;
    float moveSpeed = 10;

    private void Start()
    {
        StartCoroutine(DestroyBulletsServerRpc());
        moveDir = -transform.up;
    }

    [ServerRpc]
    IEnumerator DestroyBulletsServerRpc()
    {
        yield return new WaitForSeconds(3);
        if(!gameObject)
            Destroy(gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        transform.position += moveDir.normalized * moveSpeed * Time.deltaTime;
    }
}
