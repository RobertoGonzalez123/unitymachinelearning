using System.Collections;
using System.Collections.Generic;
using Unity.Netcode;
using UnityEngine;
using UnityEngine.UI;

public class NetworkManagerUI : MonoBehaviour
{
    public Button Server;
    public Button Host;
    public Button Client;

    private void Awake()
    {
        Server.onClick.AddListener(() => {
            NetworkManager.Singleton.StartServer();
        });
        Client.onClick.AddListener(() => {
            NetworkManager.Singleton.StartClient();
        });
        Host.onClick.AddListener(() => {
            NetworkManager.Singleton.StartHost();
        });
    }

}
